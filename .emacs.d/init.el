;;; init.el --- My Emacs configuration

;; Copyright (C) 2020 Rash Pro

;; Author: Rash Pro<rash.pro@gmail.com>
;; Created: 18 Feb 2020
;; Homepage: https://gitlab.com/rash_pro/dotfiles
;; Keywords: abbrev, convenience, faces, maint, outlines, vc

;; This program is free software. You can redistribute it and/or modify it under
;; the terms of the Do What The Fuck You Want To Public License, version 2 as
;; published by Sam Hocevar.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.
;;
;; You should have received a copy of the Do What The Fuck You Want To Public
;; License along with this program. If not, see http://www.wtfpl.net/.

;;; Commentary:

;; Following lines load an Org file and build the configuration code out of it.

;;; Code:
(require 'package)
(setq-default
  load-prefer-newer t
  package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)
;; Install dependencies
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package t))
  (setq-default
   use-package-always-defer t
   use-package-always-ensure t)

  ;; Use latest Org
(use-package org :ensure org-plus-contrib)

;; Tangle config
(org-babel-load-file "~/.emacs.d/dotemacs.org")

;;; init.el ends here

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("96bfc952ae7c93984085406c44d9ec3b98784a9de445d35958588ac9e303af1d" default))
 '(helm-command-prefix-key "C-;")
 '(ivy-count-format "")
 '(ivy-display-style nil)
 '(ivy-format-functions-alist '((t . ivy-format-function-line)))
 '(ivy-initial-inputs-alist nil)
 '(ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-center)))
 '(ivy-posframe-height-alist '((t . 24)))
 '(ivy-posframe-parameters
   '((alpha . 90)
     (border-width . 0)
     (internal-border-width . 16)))
 '(ivy-re-builders-alist '((t . ivy--regex-plus)) t)
 '(package-selected-packages
   '(dockerfile-mode docker-mode yaml-mode org-inlinetask org-bullets pug-mode beacon delight haskell-mode all-the-icons-dired ssh-agency coffee-mode slim-mode which-key undo-tree helm-files helm-config crux telephone-line nyan-mode smart-mode-line counsel-projectile counsel web-mode use-package swiper srcery-theme rust-mode org-plus-contrib magit js2-mode ivy-rich ivy-posframe helm))
 '(swiper-goto-start-of-match t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-level-1 ((t (:inherit outline-1 :height 1.0))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.0))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.0))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.0)))))
